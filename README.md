# TheScore NFL Viewer

## Description

The original problem statement is presented below.

Briefly, I was asked to create a web page that displays tabulated data. The source of this data is a json file. The application is to allow the user to filter, search and export the data to CSV.

I decided to use a Ruby on Rails since this is the framework I'm currently most comfortable with. The goal was to use as much existing code as possible:

- to decreate "time to market"
- to increase reuse
- to avoid bugs

Although this is a toy/demo project I approached it as I would I real project.

I wanted the code to be:

- testable
- simple
- extendable

The process I followed (also see `git log`):

- [define a model](app/models/player.rb) for player data and a way to import it from JSON. I used a minimal [set of tests](test/models/player_test.rb) to verify this functionality.
- investigate a way to display tabulated data. This is a common task and indeed there was a gem, [Datagrid](https://github.com/bogdan/datagrid), that fit the requirements neatly.
- create a rake task ([`thescore.rake`](lib/tasks/thescore.rake)) to populate the local application database from the json file.
- [implement CSV export](app/controllers/players_controller.rb#L30). DataGrid claims to support CSV export but it was failing. Normally I would investigate, but in the interest of expediency I created a simple exporter.



## Running && Deployment


### Docker

For deployment, I decided to use a very simple docker container. There are many advantages to distributing and deploying via docker. For this demo the purpose of the container is to allow the team to run the container localy without having to install all of the dependencies.


### In development

To run this in development, a fully enabled Ruby on Rails environment is required, with Ruby 2.5.5.

- Install bundler: `gem install bundler`
- Install Rails environment: `bundle`
- Seed the database from rushing.json: `rake thescore:seed`
- Run rails: `rails s`

You should be able to access the application on port 3000 on the local server: http://localhost:3000

### Containerized

Provided you have docker install the following should be sufficient:

```
  docker run -it -p 8080:3000 registry.gitlab.com/dmitrym0/nfl-rushing:5
```

Application will start on port 8080.

## Deployed

You can optionally access the deployed application at http://thescore.duckdns.org

## Guide

See guide below on how to use the application.

![Guide](guide.png "Guide")










# Original Content Below
# theScore "the Rush" Interview Question
At theScore, we are always looking for intelligent, resourceful, full-stack developers to join our growing team. To help us evaluate new talent, we have created this take-home interview question. This question should take you no more than a few hours.

**All candidates must complete this before the possibility of an in-person interview. During the in-person interview, your submitted project will be used as the base for further extensions.**

### Why a take-home interview?
In-person coding interviews can be stressful and can hide some people's full potential. A take-home gives you a chance work in a less stressful environment and showcase your talent.

We want you to be at your best and most comfortable.

### A bit about our tech stack
As outlined in our job description, you will come across technologies which include a server-side web framework (either Ruby on Rails or a modern Javascript framework) and a front-end Javascript framework (like ReactJS)

### Understanding the problem
In this repo is the file [`rushing.json`](/rushing.json). It contains data about NFL players' rushing statistics. Each entry contains the following information
* `Player` (Player's name)
* `Team` (Player's team abreviation)
* `Pos` (Player's postion)
* `Att/G` (Rushing Attempts Per Game Average)
* `Att` (Rushing Attempts)
* `Yrds` (Total Rushing Yards)
* `Avg` (Rushing Average Yards Per Attempt)
* `Yds/G` (Rushing Yards Per Game)
* `TD` (Total Rushing Touchdowns)
* `Lng` (Longest Rush -- a `T` represents a touchdown occurred)
* `1st` (Rushing First Downs)
* `1st%` (Rushing First Down Percentage)
* `20+` (Rushing 20+ Yards Each)
* `40+` (Rushing 40+ Yards Each)
* `FUM` (Rushing Fumbles)

##### Requirements
1. Create a web app. This must be able to do the following steps
    1. Create a webpage which displays a table with the contents of `rushing.json`
    2. The user should be able to sort the players by _Total Rushing Yards_, _Longest Rush_ and _Total Rushing Touchdowns_
    3. The user should be able to filter by the player's name
    4. The user should be able to download the sorted/filtered data as a CSV

2. Update the section `Installation and running this solution` in the README file explaining how to run your code

### Submitting a solution
1. Download this repo
2. Complete the problem outlined in the `Requirements` section
3. In your personal public GitHub repo, create a new public repo with this implementation
4. Provide this link to your contact at theScore

We will evaluate you on your ability to solve the problem defined in the requirements section as well as your choice of frameworks, and general coding style.

### Help
If you have any questions regarding requirements, do not hesitate to email your contact at theScore for clarification.

### Installation and running this solution
... TODO
