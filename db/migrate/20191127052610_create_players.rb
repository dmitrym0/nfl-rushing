class CreatePlayers < ActiveRecord::Migration[5.2]
  def change
    create_table :players do |t|
      t.string :name
      t.string :team
      t.string :position
      t.float :rushing_per_game_average
      t.integer :rushing_attempts
      t.integer :total_rushing_yards
      t.integer :rushing_average_yards_per_attempt
      t.integer :rushing_yards_per_game
      t.integer :total_rushing_touchdowns
      t.string :longest_rush
      t.integer :rushing_first_downs
      t.float :rushing_first_downs_percentage
      t.integer :rushing_20_yards
      t.integer :rushing_40_yards
      t.integer :fumbles

      t.timestamps
    end
  end
end
