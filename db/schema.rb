# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_11_27_052610) do

  create_table "players", force: :cascade do |t|
    t.string "name"
    t.string "team"
    t.string "position"
    t.float "rushing_per_game_average"
    t.integer "rushing_attempts"
    t.integer "total_rushing_yards"
    t.integer "rushing_average_yards_per_attempt"
    t.integer "rushing_yards_per_game"
    t.integer "total_rushing_touchdowns"
    t.string "longest_rush"
    t.integer "rushing_first_downs"
    t.float "rushing_first_downs_percentage"
    t.integer "rushing_20_yards"
    t.integer "rushing_40_yards"
    t.integer "fumbles"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
