class PlayersGrid < BaseGrid

  scope do
    Player
  end

  filter(:name, :string)
  filter(:team, :string)



  
  column(:name)
  column(:team)
  column(:position)
  column(:rushing_per_game_average)
  column(:rushing_attempts)
  column(:total_rushing_yards)
  column(:rushing_average_yards_per_attempt)
  column(:rushing_yards_per_game)
  column(:total_rushing_touchdowns)
  column(:longest_rush)
  column(:rushing_first_downs)
  column(:rushing_first_downs_percentage)
  column(:rushing_20_yards)
  column(:rushing_40_yards)
  column(:fumbles)
end
