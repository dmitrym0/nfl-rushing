require 'csv'
class PlayersController < ApplicationController

  # main entry method into the player table
  def index
    @grid = PlayersGrid.new(grid_params) do |scope|
      scope.page(params[:page])
    end


    respond_to do |format|
      # for html we just return the default handler
      format.html
      # for csv we build based on current view
      format.csv {
        send_data to_cvs(@grid.assets),
          filename: "export.csv",
          type: 'text/csv; charset=utf-8' 
    }
    end
  end
  
  protected

  def grid_params
    params.fetch(:players_grid, {}).permit!
  end

  # convert active recrod to csv
  def to_cvs(assets)
    csv_string = CSV.generate do |csv|
      csv << Player.attribute_names
      assets.each do |player|
        csv << player.attributes.values
      end
    end
  end
end
