class Player < ApplicationRecord

  # this is how we map from the key values in json to attribute names in our model.
  JSON_TO_ATTRIBUTE_MAP = {
    "Player" => "name",
    "Team" => "team",
    "Pos" => "position",
    "Att/G" => "rushing_per_game_average",
    "Att" => "rushing_attempts",
    "Yds" => "total_rushing_yards",
    "Avg" =>  "rushing_average_yards_per_attempt",
    "Yds/G" => "rushing_yards_per_game",
    "TD" => "total_rushing_touchdowns",
    "Lng" => "longest_rush",
    "1st" => "rushing_first_downs",
    "1st%" => "rushing_first_downs_percentage",
    "20+" =>  "rushing_20_yards",
    "40+" => "rushing_40_yards",
    "FUM" => "fumbles",
  }

  # a helper to convert json record into an ActiveRecord
  def self.from_json(json = nil)
    raise if json == nil
    player = Player.new
    JSON_TO_ATTRIBUTE_MAP.each { |json_attr, model_key| player.update_attribute(model_key, json[json_attr]) }
    player
  end  
end
