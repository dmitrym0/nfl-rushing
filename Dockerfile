FROM ruby:2.5.5

# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

WORKDIR /usr/src/app

COPY Gemfile Gemfile.lock ./

# we need bundler 2.0 and the image doesn't come with it
RUN gem install bundler
# get git so we can install dependencies
RUN apt-get update && apt-get install -y git libv8-dev -qq  build-essential nodejs --fix-missing --no-install-recommends

RUN bundle install

COPY . .

CMD ["./run.sh"]
