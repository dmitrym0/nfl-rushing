require 'active_record'
require 'rake'
namespace :thescore do
  desc "Import data from rushing.json"
  task :seed => :environment do
    # yup, YAML and JSON use the same parser.
    json = YAML.load_file("rushing.json")
    json.each { |entry| Player.from_json(entry) }
  end
end
