require 'test_helper'

class PlayerTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end


  test "empty initialization throws exception" do
    assert_raises(RuntimeError) { Player.from_json}
  end

FIXTURE = <<~HEREDOC
{
    "Player":"Joe Banyard",
    "Team":"JAX",
    "Pos":"RB",
    "Att":2,
    "Att/G":2,
    "Yds":7,
    "Avg":3.5,
    "Yds/G":7,
    "TD":0,
    "Lng":"7",
    "1st":0,
    "1st%":0,
    "20+":0,
    "40+":0,
    "FUM":0
  }
HEREDOC


  test "single json record doesn't throw an exception" do
    assert_nothing_raised do
      Player.from_json(JSON.parse FIXTURE)
    end
  end

  test "json attributes to key mapping" do
    assert_equal "name", Player::JSON_TO_ATTRIBUTE_MAP["Player"], "Expectin \"player\" attribute in json to correspond to :name key in the model"
  end

  test "get teh correct team and player name from a fixture" do
    p = Player.from_json(JSON.parse FIXTURE)

    assert_equal "Joe Banyard", p.name, "Expecting this naem!"
    assert_equal "JAX", p.team, "Expecting this team!"
  end
  
end
